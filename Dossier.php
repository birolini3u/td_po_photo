<?php
class Dossier{
	private $parent;
	private $titre;
	private $photos;
	public function __construct($nom,$id,$photo=null){
		$this->parent=$id;
		$this->titre=$nom;
		$this->photos=$photo;
	}
	
	public function getPhoto(){
		return $this->photos;
	}
	public function getTitre(){
		return $this->titre;
	}
	public function getParent(){
		return $this->parent;
	}
}
?>


