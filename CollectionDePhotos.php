<?php

class CollectionDePhotos{
	private $description;
	private $dossiers;	
	public function __construct($des , $dos){
	$this->description=$des;
	$this->dossiers=$dos;
	}
	
	public function photos($id){
		
		if (isset($this->dossiers[$id])) {
			$res=$this->dossiers[$id]->getPhoto();
			return $res;
		}
		else{
		 	return array();
		}	
	}
	
	public function chemin($id) {
		if (isset($this->dossiers[$id])) {
			$chaine="";
			$dossier_current=$this->dossiers[$id];
			while ($dossier_current->getParent()!=-1){
				$chaine=$dossier_current->getTitre()."/".$chaine;
				$parent_id=$dossier_current->getParent();
				$dossier_current=$this->dossiers[$parent_id];
			}
		$chaine="images/".$dossier_current->getTitre()."/".$chaine;
		return $chaine;
		}
		else return null;
			
	}
	private function enfant($id){
		$tab_res=array();
		foreach ( $this->dossiers as $d){
			if ($d->getParent()==$id) {
				$tab_res[]=$d->getTitre();
				
			}
		}
		return $tab_res;	
	}
	public function sousdossier($id){
		$tab_res=array();
		if(isset($this->dossiers[$id])){
			$tab_tmp=$this->enfant($id);
			
			$tab_res=array_merge($tab_tmp);
			foreach ( $tab_res as $k=>$r){
				unset($tab_tmp[$k]);
			}
			return $tab_res;
		}
	}

	public function affichage($id){
		$html="<div> Dans le dossier ".$this->dossiers[$id]->getTitre()." :</div>";
		$res=$this->sousdossier($id);
		if(count($res) >=1){
			$html.="<p>Les sous dossiers sont $res[0] et $res[1]</p>";
			
		}
		else{
			$html.="<p>Aucun sous dossier present !</p>";
			$res_photo=$this->dossiers[$id]->getPhoto();
			foreach ( $res_photo as $key => $r){
				$html.="<img src=".$this->chemin($id).$key."><p>".$key."</p><br>";
			}
		}
		return $html;
	}
}


?>
